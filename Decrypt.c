/**
* A program that gets an encripted string and a "caesar cipher" shift,
* and prints the decrypted string.
*/

#include <stdio.h>
#include <ctype.h>

#define ALPHABET_SIZE 26
#define A_ASCII_VAL 'A'
#define MAX_STRING_SIZE 100


/** a struct for a pair of "caesar cipher" shift and a string. */
typedef struct DecryptPair 
{
	int shift;
	char str[MAX_STRING_SIZE + 1];

} DecryptPair;


/** 
* gets struct of "caesar cipher" shift and encrypted string.
* decrypts the string in-place.
*/
void decryption(DecryptPair* dp)
{
	int i = 0;
	int posShift = dp->shift;

	if (dp->shift < 0)
	{
		posShift = dp->shift + ALPHABET_SIZE;
	}

	while (dp->str[i] != '\0') 
	{
		/* the formula of getting the letter after shift. */
		dp->str[i] = A_ASCII_VAL + (dp->str[i] - A_ASCII_VAL + posShift) % ALPHABET_SIZE;

		i++;
	}
}


/** returns -1 iff str has non-upper case letter, and 0 otherwise. */
int hasNonUpperLetter(const char* str)
{
	int i = 0;

	while (str[i] != '\0')
	{
		if (!isupper(str[i]))
		{
			return -1;
		}

		i++;
	}

	return 0;
}

/** 
* the main.
* gets encripted string and a "caesar cipher" shift,
* and prints decrypted string.
*/
int main() 
{
	DecryptPair dp;
	
	printf("Enter Shift: ");
	scanf("%d", &dp.shift);

	if ((dp.shift < -26) || (dp.shift > 26))
	{
		printf("INVALID\n");
		return 1;
	}

	printf("Enter String to Decrypt: ");
	scanf("%s", dp.str);

	if (hasNonUpperLetter(dp.str) == -1) 
	{
		printf("INVALID\n");
		return 1;
	}
	
	decryption(&dp);  /* perform decryption. */
	printf("Decrypted String: %s", dp.str);

	return 0;
}
