/**
* A program that gets 2 strings from the user,
* compares them lexicographically, and prints the result.
*/

#include <stdio.h>

#define STR_MAX_LEN 30
#define INVALID_STRING_MSG "STRING %d is INVALID\n"

/** returns the lenght of str. */
int getStrLen(const char* str)
{
	int i = 0;

	while (str[i] != '\0') 
	{
		i++;
	}
	
	return i;
}

/** 
* compares strings str1 and str2 lexicographically. 
* returns 0 if equal, 1 if str1 is bigger, -1 if str2 is bigger. 
*/
int compareStrings(const char* str1, const char* str2)
{
	int i;

	while((str1[i] != '\0') && (str2[i] != '\0'))
	{

		if (str1[i] > str2[i])   /* str1 is bigger. */
		{
			return 1;
		}

		if (str1[i] < str2[i])   /* str2 is bigger. */
		{
			return -1;
		}

		i++;
	}

	if ((str1[i] != '\0') && (str2[i] == '\0'))   /* str1 is bigger. */
	{
		return 1;
	}

	if ((str1[i] == '\0') && (str2[i] != '\0'))   /* str2 is bigger. */
	{
		return -1;
	}

	return 0;  /* both reached \0 at the same time - equal. */
}


/** removes the \n at end of string, if any (assumnes that string length > 0). */
void removeNewLineAtEndOfString(char* str) 
{
	if ((str == NULL) || (getStrLen(str) == 0))
	{
		return;
	}

	if (str[getStrLen(str) - 1] = '\n')
	{
		str[getStrLen(str) - 1] = '\0';  
	}
}


/** 
* gets 2 input strings from user.
*/
void getUserInput(char* str1, char* str2)
{
	printf("Enter string 1: ");

	/* we read a bit more than max. str. size, to check invalid strings. */
	fgets(str1, STR_MAX_LEN + 2, stdin);  

	fflush(stdin);

	removeNewLineAtEndOfString(str1);  /* remove the \n that fgets may add. */


	printf("Enter string 2: ");

	/* we read a bit more than max. str. size, to check invalid strings. */
	fgets(str2, STR_MAX_LEN + 2, stdin);

	fflush(stdin);

	removeNewLineAtEndOfString(str2);  /* remove the \n that fgets may add. */
}

/** 
* gets numeric result of strings comparison, 
* and prints an appropriate message.
*/
void printSrtComparisonResMsg(int compRes)
{

	if (compRes == 0)
	{
		printf("Strings are equal\n");
	}

	if (compRes == 1)
	{
		printf("String 1 is greater\n");
	}

	if (compRes == -1)
	{
		printf("String 2 is greater\n");
	}

}


/** 
* the main.
* gets 2 strings from user, and prints the result of their comparison.
*/
int main() 
{
	char str1[STR_MAX_LEN + 2];
	char str2[STR_MAX_LEN + 2];
	int validStrFlag, res;

	getUserInput(str1, str2);

	validStrFlag = 1;

	if (getStrLen(str1) > STR_MAX_LEN)
	{
		printf(INVALID_STRING_MSG, 1);
		validStrFlag = 0;
	}

	if (getStrLen(str2) > STR_MAX_LEN)
	{
		printf(INVALID_STRING_MSG, 2);
		validStrFlag = 0;
	}

	if (validStrFlag == 1)  /* both strings are valid. */
	{
		res = compareStrings(str1, str2);

		printSrtComparisonResMsg(res);
	}
	
	return 0;
}
