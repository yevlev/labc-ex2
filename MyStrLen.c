/**
* A program that gets a string from the command line,
* and returns its length.
*/

#include <stdio.h>

#define STR_MAX_LEN 25
#define INVALID_STRING_MSG "INVALID STRING TOO LONG!\n"

/** returns the lenght of str, or -1 if its length is more than 25. */
int getStrLength(const char* str)
{
	int i = 0;

	while (str[i] != '\0') 
	{
		i++;

		if (i > STR_MAX_LEN)
		{
			return -1;
		}
	}
	
	return i;
}

/** 
* the main.
* gets string from command line, and prints its length.
*/
int main(int argc, char** argv) 
{
	char* inputString;
	int res;

	if (argc != 2) 
	{
		fprintf(stderr, "Wrong number of arguments to main - should be: a single string\n");
		return 1;
	}

	inputString = argv[1];

	res = getStrLength(inputString);

	if (res == -1)   /* string is too long. */
	{
		printf(INVALID_STRING_MSG);
		return 1; 
	}

	printf("%d\n", res);   /* the string length. */
	
	return 0;
}
