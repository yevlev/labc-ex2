/**
* A program that gets a path to file, where each line
* contains a file name, and on which files it depends,
* and prints if there is/isn't cyclic dependency.
*/

#include <stdio.h>
#include <string.h>

#define MAX_NUM_FILES 1001
#define MAX_DEPENDENT_FILES 101
#define MAX_FILE_NAME_LENGTH 65
#define MAX_LINE_LEN 1001


/** dfs vertex color. */
typedef enum {WHITE, GRAY, BLACK} color_t;  


/** 
* struct that holds info. about single file:
* its name and array of file on which it depends.
*/
typedef struct FileStruct 
{
	char name[MAX_FILE_NAME_LENGTH];
	struct FileStruct* deps[MAX_DEPENDENT_FILES];
	int lastInd;  /* index of 1st empty cell. */
	color_t color;   /* for dfs algo. */

} FileStruct;


/** struct that holds dependencies array (for all files) and its size. */
typedef struct DepArrayStruct 
{
	FileStruct dependencies[MAX_NUM_FILES];
	int lastInd;  /* index of 1st empty cell. */

} DepArrayStruct;


/** removes the \n at end of string, if any. */
void removeNewLineAtEndOfString(char* str) 
{
	if (strlen(str) == 0)
	{
		return;
	}

	if (str[strlen(str) - 1] = '\n')
	{
		str[strlen(str) - 1] = '\0';  
	}
}


/** 
* searches for a file by its name in dependencies array.
* returns the index of the file in the array if found, or -1 otherwise.
*/
int findFileName(const DepArrayStruct* dep, const char* fileName) 
{
	int i;

	for (i = 0; i < dep->lastInd; i++ )
	{
		if (strcmp(dep->dependencies[i].name, fileName) == 0) /* found file. */
		{
			return i;
		}
	}

	return -1;
}

/**
* returns 1 iff a file with fileName exists in array of dependant files of given FileStruct
*/
int findDependentFile(const FileStruct fs, const char* fileName)
{
	int i;

	for (i = 0; i < fs.lastInd; i++)
	{
		if (strcmp(fs.deps[i]->name, fileName) == 0)   /* found fileName in dep. arr. */
		{
			return 1;
		}
	}

	return 0;
}


/** adds file named fileName into dependencies array. */
void addToDeps (DepArrayStruct* dep, const char* fileName)
{
	strncpy(dep->dependencies[dep->lastInd].name, fileName, strlen(fileName) + 1);
	dep->dependencies[dep->lastInd].lastInd = 0;
	dep->lastInd++;
}


/**
* gets a line of input file, parses it, and adds the data to dependencies array.
*/
void parseInputFileLine(DepArrayStruct* das, char* line)
{
	char* depFileName;
	int lhsFileInd = 0;
	int indInDepsArray = 0;
	char* fileName;
	char* deps;
	
	/* getting left hand side file name and string of files on which it depends. */
	fileName = strtok(line, ": ");
	deps = strtok(NULL, ": ");
	
	/* checking if fileName already exists in dependencies array. */
	lhsFileInd = findFileName(das, fileName);
	if (lhsFileInd == -1)  /* not found. */
	{
		addToDeps(das, fileName);  /* adding to dependencies array (in last ind.). */
		lhsFileInd = das->lastInd - 1;
	}
		
	/* parsing the dependent files. */

	depFileName = strtok(deps, ",");

	while (depFileName != NULL) 
	{
		/* checks if curr. dependent file already exists in arr. of dep. file of lhs file. */
		if (!findDependentFile(das->dependencies[lhsFileInd], depFileName)) 
		{
			indInDepsArray = findFileName(das, depFileName);  /* location of dependent file in dependencies arr. */

			if (indInDepsArray == -1)  /* dependent file not found in dependencies arr. */
			{
				addToDeps(das, depFileName);   /* adding dependent file to dependencies arr. */
				indInDepsArray = das->lastInd - 1;
			}

			/* adds dependent file to dep. array of lhs file. */
			das->dependencies[lhsFileInd].deps[das->dependencies[lhsFileInd].lastInd] = &(das->dependencies[indInDepsArray]); 
			das->dependencies[lhsFileInd].lastInd++;
		}

		depFileName = strtok(NULL, ",");  /* next dependent file. */

	}  /* end of line parsing while. */
}

/** 
* parses the input file and fills the dependencies struct.
* in case of failure returns -1.
*/
int parseInputFile(const char* path, DepArrayStruct* das)
{
	char line[MAX_LINE_LEN];
	FILE* fp = fopen(path, "r"); 

	if (fp == NULL) 
	{   
		printf("Error: file could not be opened or the path/file doesn't exist: %s\n", path);
		return -1;
	}

	while(!feof(fp)) 
	{
		line[0] = '\0';

		fgets(line, MAX_LINE_LEN, fp); /* reads a line from file. */

		removeNewLineAtEndOfString(line);

		if (ferror(fp))
		{
			printf("Error: reading from file failed: %s\n", path);
			fclose(fp);
			return -1;
		}

		if (strlen(line) == 0)  /* empty line was read. */
		{
			continue;
		}

		parseInputFileLine(das, line);
		
	} /* end of reading from file while. */

	fclose(fp);

	return 0;
}


/** 
* the dfs visit recursive function.
* hasBackwardEdge is set to 1 if backward edge found.
*/
void dfsVisit(FileStruct* fs, int* hasBackwardEdge)
{
	int i;
	
	if (*hasBackwardEdge == 1) 
	{
		return;
	}

	fs->color = GRAY;

	for (i = 0; i < fs->lastInd ; i++)
	{
		if (fs->deps[i]->color == WHITE)
		{
			dfsVisit(fs->deps[i], hasBackwardEdge);  /* recursive call. */
		}

		if (fs->deps[i]->color == GRAY)  /* backward edge found - graph is cyclic. */
		{
			*hasBackwardEdge = 1;
			return;
		}

	}

	fs->color = BLACK;
}

/** 
* performs the dfs algo. on dependencies graph.
* returns 1 if backward edge was found, and 0 otherwise.
*/
int dfs(DepArrayStruct* das)
{
	int i;
	int hasBackwardEdge = 0; /* the default vales is no back edge. */

	for (i = 0; i < das->lastInd; i++)
	{
		das->dependencies[i].color = WHITE;  /* dfs init. stage. */
	}

	/* iterate through all vertices and call dfsVisit on them. */
	for (i = 0; i < das->lastInd; i++) 
	{
		if (das->dependencies[i].color == WHITE)
		{
			dfsVisit (&(das->dependencies[i]), &hasBackwardEdge);
		}

		if (hasBackwardEdge == 1) 
		{
			return 1;
		}
	}

	return 0;
}


/** 
* the main.
* envokes parsing of input file, and then checking if
* there is cyclic deoendency. prints the result.
*/
int main(int argc, char** argv) 
{
	DepArrayStruct das;
	char* filePath;
	int res;

	if (argc != 2) 
	{
		fprintf(stderr, "Wrong number of arguments to main - should be: file path\n");
		return 1;
	}

	filePath = argv[1];
	
	das.lastInd = 0;  /* init. */

	res = parseInputFile(filePath, &das);  /* filling the struct from file. */
	
	if (res == -1)  /* error in parsing. */
	{ 
		return 1;
	}

	res = dfs(&das);

	if (res == 1)  /* backward edge was found. */
	{
		printf("Cyclic Dependencies Exist\n");
	}
	else 
	{
		printf("No Cyclic Dependencies Exist\n");
	}
	
	return 0;
}